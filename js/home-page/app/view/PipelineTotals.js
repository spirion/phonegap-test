define(['tpl!template/PipelineTotals',
        'view/NavPanel',
        'model/PipelineTotalsCollection',
        'backbone-marionette'],
        
function (initialTemplate,
		  NavPanel,
		  PipelineTotalsCollection,
		  Marionette) {
	'use strict';
	return Marionette.LayoutView.extend({
		
		template: initialTemplate,
		collection : new PipelineTotalsCollection(),
		
		regions : {
			'nav-panel' : 'div#nav-panel'
		},
		
		ui : {
			calendar : 'div#calendar'
		},
		
		onRender : function() {
			this.getRegion('nav-panel').show(new NavPanel({
				current : 'pipeline-totals'
			}));
			this._renderCalendar();
		},
		
		_renderCalendar : function () {
			var self = this;
			this.collection.fetch()
				.done(function(data, textStatus, jqXHR) {
					self.ui.calendar.fullCalendar({
						events : self.collection.toJSON(),
						eventColor:"#c71444",
						eventAfterAllRender : function(view) {
							$('.fc-scroller', self.ui.calendar).css({'height':'100%','overflow-y':'hidden'});
						}
					});
					$('button', self.ui.calendar).addClass('ui-btn ui-btn-inline');
				});
		},
		
		onShow : function() {
			console.log('onShow');
		},
		
		today : function() {
			this.ui.calendar.fullCalendar('today');
		}
			
	});
	
});