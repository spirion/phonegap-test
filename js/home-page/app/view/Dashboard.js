define(['tpl!template/Dashboard',
        'view/NavPanel',
        'backbone-marionette'],
        
function (initialTemplate,
		  NavPanel,
		  Marionette) {
	'use strict';
	return Marionette.LayoutView.extend({
		
		template: initialTemplate,
		
		regions : {
			'nav-panel' : 'div#nav-panel'
		},
		
		onRender : function() {
			this.getRegion('nav-panel').show(new NavPanel({
				current : 'dashboard'
			}));
		}
			
	});
	
});