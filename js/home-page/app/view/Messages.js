define(['tpl!template/Messages',
        'view/NavPanel',
        'view/MessageList',
        'model/MessageCollection',
        'backbone-marionette'],
        
function (initialTemplate,
		  NavPanel,
		  MessageList,
		  MessageCollection,
		  Marionette) {
	'use strict';
	return Marionette.LayoutView.extend({
		
		template: initialTemplate,
		collection : new MessageCollection(),
		
		ui : {
			messageList : 'div#message-list'
		},
		
		regions : {
			'nav-panel' : 'div#nav-panel',
			'message-list' : 'div#message-list'
		},
		
		onRender : function() {
			this.getRegion('nav-panel').show(new NavPanel({
				current : 'messages'
			}));
			this._loadMessages();
		},
		
		/*
		 * Load the user specific messages.
		 */
		_loadMessages : function() {
			var self = this;
			this._loading(true);
			setTimeout(function() {
				self.collection.fetch()
					.done(function(data, textStatus, jqXHR) {
						self.getRegion('message-list').show(new MessageList({ collection : self.collection }));
						self.ui.messageList.enhanceWithin();
					})
					.always(function() {
						self._loading(false);
					});
			}, 1000);
		},
		
		_loading : function(show) {
			if (show) {
				setTimeout(function() {
					$.mobile.loading('show');
				}, 1);
			} else {
				setTimeout(function() {
					$.mobile.loading('hide');
				}, 1);
			}
		}
			
	});
	
});