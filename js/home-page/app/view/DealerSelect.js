define(['tpl!template/DealerSelect', 
        'backbone-marionette'],
        
function (initialTemplate,
		  Marionette) {
	'use strict';
	return Marionette.LayoutView.extend({
		
		template: initialTemplate,
		
		ui : {
			nextButton : 'button#next'
		},
		
		events : {
			'click @ui.nextButton' : '_loadMessages',
		},
		
		_loadMessages : function() {
			location.hash = 'messages';
		}
			
	});
	
});