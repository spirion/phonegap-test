define(['tpl!template/NavPanel', 
        'backbone-marionette'],
        
function (initialTemplate,
		  Marionette) {
	'use strict';
	return Marionette.LayoutView.extend({
		
		template: initialTemplate,
			
		_currentSelection : null,
		
		ui : {
			navPanel : 'div#nav-panel'
		},
		
		initialize : function(options) {
			this._currentSelection = options.current;
		},
		
		onRender : function() {
			if (this._currentSelection) {
				$('li#' + this._currentSelection + ' a', this.$el).hide();
			}
		}

	});
	
});