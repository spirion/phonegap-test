define(['tpl!template/MessageList',
        'view/Message',
        'backbone-marionette'],
        
function (initialTemplate,
		  Message,
		  Marionette) {
	'use strict';
	
	return Marionette.CollectionView.extend({
		
		template: initialTemplate,
		childView : Message,
		tagName : 'ul',
		attributes : {
			'data-role' : 'listview'
		},
		
	});
	
});