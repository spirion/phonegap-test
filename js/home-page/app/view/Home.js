define(['tpl!template/Home', 
        'backbone-marionette',
        'view/Login'],
        
function (initialTemplate,
		  Marionette,
		  Login) {
	'use strict';
	return Marionette.LayoutView.extend({
		
		template: initialTemplate,
		
		regions : {
			login : 'div#login'
		},
		
		onRender : function() {
			this.getRegion('login').show(new Login());
		},
		
		onDestroy : function() {
			
		}
		
	});
	
});