define(['tpl!template/Message',
        'backbone-marionette'],
        
function (initialTemplate,
		  Marionette) {
	'use strict';
	
	return Marionette.ItemView.extend({
		
		template: initialTemplate,
		tagName : 'li',
		attributes : function() {
			if (this.model.get('priority') === 'HIGH') {
				return {
					'data-theme' : 'a'
				};
			}
		},
		
		ui : {
			popup : 'div#message-popup'
		},
		
		events : {
			'click div.message-subject' : 'showMessage'
		},
		
		showMessage : function() {
			this.ui.popup.popup('open');
		}
		
	});
	
});