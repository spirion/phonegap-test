define(['tpl!template/Login', 
        'backbone-marionette'],
        
function (initialTemplate,
		  Marionette) {
	'use strict';
	return Marionette.LayoutView.extend({
		
		template: initialTemplate,
		
		ui : {
			loginButton : 'button#login'
		},
		
		events : {
			'click @ui.loginButton' : '_login',
		},
		
		_login : function() {
			var self = this;
			this._loading(true);
			setTimeout(function() {
				location.hash = 'dealer-select';
				self._loading(false);
			}, 1000);
		},
		
		_loading : function(show) {
			if (show) {
				setTimeout(function() {
					$.mobile.loading('show');
				}, 1);
			} else {
				setTimeout(function() {
					$.mobile.loading('hide');
				}, 1);
			}
		}
		
	});
	
});