define(['view/Home',
        'view/DealerSelect',
        'view/Dashboard',
        'view/Messages',
        'view/PipelineTotals',
        'backbone-marionette',
        'fullcalendar'],

function(Home,
		 DealerSelect,
		 Dashboard,
		 Messages,
		 PipelineTotals) {
	'use strict';
	
	/*
	 * Add a global notifications object for registering global events.
	 */
	Backbone.Notifications = {};
	_.extend(Backbone.Notifications, Backbone.Events);
	
	/*
	 * Initialise the app router
	 */
	var AppRouter = Backbone.Router.extend({
			routes:{
				'' : 'home',
				'dealer-select' : 'dealerSelect',
				'dashboard' : 'dashboard',
				'messages' : 'messages',
				'pipeline-totals' : 'pipelineTotals'
			},
			
			initialize:function () {
				$('.back').on('click', function(event) {
					window.history.back();
					return false;
				});
				this.firstPage = true;
			},
			
			currentView : null,
			
			changePage : function (page) {
				var transition = $.mobile.defaultPageTransition;
				if (this.currentView) {
					this.currentView.remove();
				}
				this.currentView = page;
				$(page.el).attr('data-role', 'page');
				page.render();
				$('body').append($(page.el));
				if (this.firstPage) {
					transition = 'none';
					this.firstPage = false;
				}
				$.mobile.changePage($(page.el), { changeHash : false, transition : transition });
			},
			
			home : function() {
				this.changePage(new Home());
			},
			
			dealerSelect : function() {
				this.changePage(new DealerSelect());
			},
			
			dashboard : function() {
				this.changePage(new Dashboard());
			},
			
			messages : function() {
				this.changePage(new Messages());
			},
			
			pipelineTotals : function() {
				var view = new PipelineTotals();
				$(document).one('pagechange', function(event) {
					view.today();
				});
				this.changePage(view);
			}
			
		}); 
	
	/*
	 * Initialise and start the application.
	 */
	new AppRouter();
	Backbone.history.start();
	Backbone.Notifications.trigger('application:ready');
	
});