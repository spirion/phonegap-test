define(['backbone-marionette'], function() {
	'use strict';
	return Backbone.Model.extend();
});