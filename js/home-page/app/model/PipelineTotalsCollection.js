define(["model/MessageItem"], function(PipelineTotalsItem) {
	'use strict';
	return Backbone.Collection.extend({
		
		model: PipelineTotalsItem,
		
		parse: function(response, options) {
			return response.payload;
		},
		
		url : function() {
			return 'mock/pipeline.json';
		}
		
	});
	
});