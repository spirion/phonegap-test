define(["model/MessageItem"], function(MessageItem) {
	'use strict';
	return Backbone.Collection.extend({
		
		model: MessageItem,
		
		parse: function(response, options) {
			return response.payload;
		},
		
		url : function() {
			return 'mock/messages.json';
		}
		
	});
	
});