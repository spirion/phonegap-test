<div class="message-item">
	<div class="message-subject">
		<%=priority === 'HIGH' ? '<span class="ui-btn ui-shadow ui-corner-all ui-icon-star ui-btn-icon-notext ui-btn-inline"></span>' : ''%>
		<%=appMessage.subject%>
	</div>
	<div data-role="popup" id="message-popup" data-overlay-theme="b" data-theme="a" data-dismissible="false" style="max-width:400px;">
		<div data-role="header" data-theme="a">
		    <h1>Message</h1>
		</div>
		<div role="main" class="ui-content">
			<div>
				<%=appMessage.message%>
			</div>
			<%if (priority === 'HIGH') {%> 
				<a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-b" data-rel="back">Dismiss</a>
			<%}%>
			<a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-a" data-rel="back">OK</a>
		</div>
	</div>
</div>