<div id="pipeline-totals-container">
	<style>
	div#pipeline-totals-container .OrderPipelineSummaryEvent {
		color : #ffffff;
		text-shadow: initial;
	}
	</style>
	<div data-role="header" data-theme="b">
    	<h1>Pipeline Totals</h1>
    	<a href="#nav-panel" data-icon="bars" data-iconpos="notext"></a>
    	<div id="nav-panel"></div>
	</div>
	<div role="main" class="ui-content jqm-content jqm-fullwidth">
		<div id="calendar"></div>
	</div>
</div>