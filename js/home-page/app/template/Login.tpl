<div>
	Log in
	<div class="ui-field-contain">
		<label for="username">User Name</label>
		<input name="username" id="username" value="" type="text"  placeholder="User Name"/>
	</div>
	<div class="ui-field-contain">
		<label for="password">Password</label>
		<input name="password" id="password" value="" type="password" placeholder="Password"/>
	</div>
	<div>
		<button id="login" class="ui-btn ui-corner-all ui-icon-user ui-btn-icon-left">Log In</button>
	</div>
</div>