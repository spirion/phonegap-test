<div>
	<div data-role="header" data-theme="b">
    	<h1>Select an Dealer</h1>
	</div>
	<div role="main" class="ui-content jqm-content jqm-fullwidth">
		<span>Select an RBU to continue</span>
		<div class="ui-field-contain">
			<label for="rbu">RBU</label>
			<select name="rbu" id="rbu">
		        <option value="1">04 - NISSAN ITALIA S.P.A.</option>
		        <option value="2">05 - NISSAN MOTOR GREAT BRITIAN</option>
		        <option value="3">21 - NISSAN MOTOR EUROPE</option>
		        <option value="4">30 - NISSAN MOTOR UKRAINE LTD</option>
		    </select>
		</div>
		<div class="ui-field-contain">
			<label for="dealer">Dealer</label>
			<select name="rbu" id="dealer">
		        <option value="1">B00001 - EUROPCAR ITALIA SPA</option>
		        <option value="2">B00002 - HERTZ ITALIANA S.P.A.</option>
		        <option value="3">B00003 - AVIS BUDGET ITALIA SPA</option>
		        <option value="4">B00004 - MAGGIORE RENT  S.P.A.</option>
		    </select>
		</div>
		<button id="next" class="ui-btn ui-corner-all ui-icon-arrow-r ui-btn-icon-right">Next</button>
	</div>
</div>