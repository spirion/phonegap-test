<div>
	<div data-role="header" data-theme="b">
    	<h1>Messages</h1>
    	<a href="#nav-panel" data-icon="bars" data-iconpos="notext"></a>
    	<div id="nav-panel"></div>
	</div>
	<div role="main" class="ui-content jqm-content jqm-fullwidth">
		<div id="message-list"></div>
	</div>
</div>