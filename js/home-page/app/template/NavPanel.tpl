<div data-role="panel" data-theme="a" data-display="push" id="nav-panel" data-position-fixed="true">
	
	<style>
	div#nav-panel a {
		font-size: 120%;
	}
	</style>
	
	<ul data-role="listview" data-theme="a">
		<div data-role="header">
	    	<h1>&nbsp;</h1>
	    </div>
		<li id="dealer-select" data-icon="check"><a href="#dealer-select">Select a dealer</a></li>
		<li id="messages" data-icon="mail"><a href="#messages">Messages</a></li>
		<li id="dashboard" data-icon="info"><a href="#dashboard">Dashboard</a></li>
		<li id="pipeline-totals" data-icon="calendar"><a href="#pipeline-totals">Pipeline Totals</a></li>
		<li></li>
		<li data-icon="user"><a href="#">Logout</a></li>
	</ul>
	
</div>