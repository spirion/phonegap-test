require.config({
    'baseUrl': 'js/home-page',
    
    'paths': {
    	'jquery': '../common/jquery',
    	'app': 'app',
    	'controller': 'app/controller',
    	'model': 'app/model',
    	'view': 'app/view',
    	'template': 'app/template',
    	'common': '../common',
    	'domReady': '../lib/domReady',
    	'underscore': '../lib/underscore',
    	'text': '../lib/text',
    	'tpl': '../lib/tpl',
    	'moment' : '../lib/moment.min',
    	'fullcalendar' : '../lib/fullcalendar',
    	'backbone': '../lib/backbone',
    	'backbone-marionette': '../lib/backbone.marionette',
    	'jquery-tmpl' : '../lib/jquery.tmpl.min',
    	'utils' : '../common/utils'
    },

    shim : {
        'underscore' : {
            exports : '_'
        },
        'backbone'   : {
            deps    : ['underscore'],
            exports : 'Backbone'
        },
        'backbone-marionette' : {
        	deps : ['backbone'],
        	exports : 'Marionette'
        }
    },
    
    'tpl': {
	    extension: '.tpl'
	}
});

// Load the main app module to start the app
require(['domReady!', 'app/main'], function() {
});